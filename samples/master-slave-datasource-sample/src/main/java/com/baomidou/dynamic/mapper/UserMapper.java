package com.baomidou.dynamic.mapper;

import com.baomidou.dynamic.datasource.DS;
import com.baomidou.dynamic.entity.User;
import org.apache.ibatis.annotations.*;

import java.util.List;

@DS
public interface UserMapper {

    @Insert("INSERT INTO user (name,age) values (#{name},#{age})")
    boolean addUser(@Param("name") String name, @Param("age") Integer age);

    @Update("UPDATE user set name=#{name}, age=#{age} where id =#{id}")
    boolean updateUser(@Param("id") Integer id, @Param("name") String name, @Param("age") Integer age);

    @Delete("DELETE from user where id =#{id}")
    boolean deleteUser(@Param("id") Integer id);

    @DS("master")
    @Select("SELECT * FROM user")
    List<User> selectAll();

}